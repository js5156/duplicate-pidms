DELETE FROM gobtpac
WHERE gobtpac_pidm = 363413;
/
DELETE FROM gorpaud
WHERE gorpaud_pidm = 363413;
/
DELETE FROM gobumap
WHERE gobumap_pidm = 363413;
/
DELETE FROM spbpers
WHERE spbpers_pidm = 363413; --No relevent data in SPBPERS record that isn't in 363414
/
UPDATE spriden
SET spriden_pidm = 363414, --new pidm
    spriden_ntyp_code = 'MID',
    spriden_id = 'T00019029', --T-id of pidm to be deleted
    spriden_change_ind = 'I',
    spriden_data_origin = 'DUPPIDM'
WHERE spriden_pidm = 363413 --pidm to delete  
AND spriden_id = 'T00019029'; --T-id of pidm to be deleted
/
DELETE FROM spriden
WHERE spriden_pidm = 363413;
/