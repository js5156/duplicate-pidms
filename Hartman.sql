DELETE FROM gobumap
WHERE gobumap_pidm = 94346;
/
DELETE FROM goremal
WHERE goremal_pidm = 94346; --1 GOREMAL record for 94346, but same record exists for 9827
/
DELETE FROM spbpers
WHERE spbpers_pidm = 94346; --No relevent data in SPBPERS record that isn't in 9827
/
UPDATE spraddr
SET spraddr_pidm = 9827
WHERE spraddr_pidm = 94346; --3 ATYPs in 94346 that don't exist for 9827, so updating
/
UPDATE spriden
SET spriden_pidm = 9827, --new pidm
    spriden_ntyp_code = 'MID',
    spriden_id = 'T24004305', --id of pidm to be merged
    spriden_change_ind = 'I',
    spriden_data_origin = 'DUPPIDM'
WHERE spriden_pidm = 94346 --pidm to delete  
AND spriden_id = 'T24004305';
/
DELETE FROM spriden
WHERE spriden_pidm = 94346;
/
UPDATE fab1099
SET fab1099_vend_pidm = 9827
WHERE fab1099_vend_pidm = 94346;
/
UPDATE fabinvh
SET fabinvh_vend_pidm = 9827
WHERE fabinvh_vend_pidm = 94346;
/
UPDATE fgbtrnh
SET fgbtrnh_vendor_pidm = 9827
WHERE fgbtrnh_vendor_pidm = 94346;
/
DELETE FROM ftvvend
WHERE ftvvend_pidm = 94346;
/
DELETE FROM ftvvent
WHERE ftvvent_pidm = 94346;
/
DELETE FROM gmxsiat
WHERE gmxsiat_pidm = 94346;
/
DELETE FROM smxiden
WHERE smxiden_pidm = 94346;
/
DELETE FROM sphiden
WHERE spriden_pidm = 94346;
/
DELETE FROM spxiden
WHERE spxiden_pidm = 94346;
/